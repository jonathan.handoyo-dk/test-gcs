FROM openjdk:17-alpine
VOLUME /tmp
ADD app.jar /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]