package com.jago.test.gcs

import com.google.cloud.storage.Storage
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.runApplication
import org.springframework.context.event.EventListener

@SpringBootApplication
class TestGcsApplication(
    private val storage: Storage
) {

    @EventListener(ApplicationReadyEvent::class)
    fun onApplicationReadyEvent(event: ApplicationReadyEvent) {
        println("Storage: $storage")
    }
}

fun main(args: Array<String>) {
    runApplication<TestGcsApplication>(*args)
}
